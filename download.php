<?php

header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=temperatures.csv");

require 'mysql.in';

$hostname = $host;
$username = $user;
$password = $pass;
$database = $base;

$dbh = mysql_connect($hostname, $username, $password);
mysql_select_db($database);

$query = "SELECT * FROM temperature WHERE 1 ORDER BY date ASC";
$info = mysql_query($query);

for ($i = 0; $data = mysql_fetch_assoc($info); $i++) {
	if ($i == 0) {
		$keys = array_keys($data);
		
		for ($j = 0; $j < sizeOf($keys); $j++) {
			echo "\"" . ucwords($keys[$j]) . "\",";
		}

		echo "\r\n";
	}

	for ($j = 0; $j < sizeOf($keys); $j++) {
		$temp = $data[$keys[$j]];

		echo "\"" . ($j != 1 ? $temp : $temp * 9 / 5 + 32 ) . "\",";
	}
	echo "\r\n";
}

mysql_close($dbh);

?>
