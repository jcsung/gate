<!DOCTYPE HTML>
<html>
<head>
<title>Gate Control</title>
<style>
body {
	font-family: Verdana, Arial, Sans-Serif;
	font-size: 48pt;
}

.control {
	margin: 100px;
}

.control .submit {
	font-size: 48pt;
	height: 100px;
	width: 200px;
}

.error {
	color: #f00;
	font-family: Courier New;
}

.flexbox-container {
	align-items: center;
	display: flex;
	flex-direction: column;
	width: 100%;
}

#form {
}

#result {
	font-size: 24pt;
}

#temperature .container {
	align-items: center;
	display: flex;
	flex-direction: column;
	font-size: 48px;
	width: 100%;
}

#temperature .container.download {
	font-size: 36px;
}

#title {
	display: block;
}
</style>
</head>
<body>
<div class="flexbox-container">
	<div id="title"><h1>Gate Control</h1></div>
<?php

//This requires wiringPi to be installed
//to use the gpio utility

$pins=array();
$pins['Open']=20;
$pins['Close']=21;

$past=array();
$past['Open']='ed';
$past['Close']='d';

if (isset($_POST['submit'])) {
	//echo print_r($_POST,true).'<br/>'."\n";
	$action=$_POST['submit'];

	if (array_search($action,array_keys($pins))===false) {
		die('<div class="error">Error: Invalid command</div>'."\n");
	}

	$time=date('Y-m-d H:i:s');
	$pastAct=$past[$action];
	echo "\t".'<div id="result">'."$action$pastAct $time</div>\n";

	$action=$pins[$action];

	$query="gpio export $action out";
	//echo "$query<br/>\n";
	$result=shell_exec("$query");
	//echo "Result: ".print_r($result,true)."<br/>\n";
	$query="gpio -g write $action 1";
	//echo "$query<br/>\n";
	$result=shell_exec("$query");
	//echo "Result: ".print_r($result,true)."<br/>\n";
	sleep(1.5);
	$query="gpio -g write $action 0";
	//echo "$query<br/>\n";
	$result=shell_exec("$query");
	//echo "Result: ".print_r($result,true)."<br/>\n";
	$query="gpio unexport $action";
	//echo "$query<br/>\n";
	$result=shell_exec("$query");
	//echo "Result: ".print_r($result,true)."<br/>\n";
}
?>
	<div id="form">
		<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
		<div class="control"><input class="submit" type="submit" name="submit" value="Open"/></div>
		<div class="control"><input class="submit" type="submit" name="submit" value="Close"/></div>	
		</form>	
	</div>

	<div id="temperature">
		<?php include 'temperature.php'; ?>
	</div>
</div>
</body>
</html>
