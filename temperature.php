<?php

require 'mysql.in';

$hostname = $host;
$username = $user;
$password = $pass;
$database = $base;

$dbh = mysql_connect($hostname, $username, $password);
mysql_select_db($database);

$query = "SELECT * FROM temperature WHERE 1 ORDER BY date DESC LIMIT 1";
$info = mysql_query($query);

$data = mysql_fetch_assoc($info);

//echo print_r($data, true)."\n";

mysql_close($dbh);

$datetime = explode(" ", $data['date']);

$datedata = explode("-", $datetime[0]);

$year = $datedata[0];
$month = $datedata[1];
$day = $datedata[2];

$timedata = explode(":", $datetime[1]);

$hour = $timedata[0];
$minutes = $timedata[1];
$seconds = $timedata[2];

$unixtime = mktime($hour, $minutes, $seconds, $month, $day, $year);

$date = date("n/j/y", $unixtime);
$time = date("g:i A", $unixtime);


$temp = $data['temperature'] * 9 / 5 + 32;

$humidity = $data['humidity'];

?>
<div class="container temperature"><?php echo $temp; ?>&deg;F</div>
<div class="container humidity">RH: <?php echo $humidity ?>%</div>
<div class="container date"><?php echo $date; ?></div>
<div class="container time"><?php echo $time; ?></div>
<div class="container download"><a href="./download.php">Download Full History</a></div>
